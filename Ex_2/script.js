'use strict';

(function() {
	var days = {
	    "01.01.2015": 33.3,
	    "02.01.2015": 20.2,
	    "03.01.2015": 18.3,
	    "04.01.2015": 22.2,
	    "05.01.2015": 30.0,
	    "05.02.2015": 10.2,
	    "06.01.2015": 40.2,
	    "07.01.2015": 22.3,
	    "08.01.2015": 23.2,
	    "09.01.2015": 24.2,
	    "10.01.2015": 25.2,
	    "11.01.2015": 30.2,
	    "12.01.2015": 30.2,
	    "13.01.2015": 31.2,
	    "14.01.2015": 10.2,
	    "14.02.2015": 10.2
	};

	var sum = 0;
	var daysCount = 0;
	var avgSum = {
		0: {
			sum: 0,
			count: 0,
		},
		1: {
			sum: 0,
			count: 0,
		}
	}
	for (var key in days) {
	    if (!days.hasOwnProperty(key)) {
	    	continue;
	    }
	    var explodeKey = key.split('.');
	    var dateObj = new Date(explodeKey[2], explodeKey[1] - 1, explodeKey[0]);
	    var weekIndex = Math.floor(dateObj.getDate() / 7);

	    if (dateObj.getMonth() != 0 || Math.floor(dateObj.getDate() / 7) > 1) {
	    	continue;
	    }
	    
	    avgSum[weekIndex].count++;
	    avgSum[weekIndex].sum += parseInt(days[key]);
	}
	console.log('През първата седмица на януари средната температура ще бъде : ', avgSum[0].sum / avgSum[0].count);
	console.log('През втората седмица на януари средната температура ще бъде : ', avgSum[1].sum / avgSum[1].count);

	console.log('------------------');

	var pastDay = null;
	for (var key in days) {
	    if (!days.hasOwnProperty(key)) {
	    	continue;
	    }
	    var explodeKey = key.split('.');
	    var dateObj = new Date(explodeKey[2], explodeKey[1] - 1, explodeKey[0]);

	    if (dateObj.getMonth() != 0) {
	    	continue;
	    }

	    if (!pastDay) {
	    	var value = days[key];
	    } else {
	    	var value = pastDay > days[key] ? (pastDay - days[key]) : (days[key] - pastDay);
	    	value = value.toFixed(2);
	    	if (value.toString() != '0.00') {
	    		value = (pastDay > days[key] ? '-' : '+') + value;
	    	}
	    }
	    pastDay = days[key];
	    console.log(key + ': ', value);
	}
 
})();