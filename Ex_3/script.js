'use strict';

(function() {
	const arr = [{date: '10.01.2017', dayOfWeek: 'Tuesday'}, {date: '05.11.2016', dayOfWeek: 'Saturday'}, {date: '21.13.2002', dayOfWeek: 'Monday'}];

	console.log(arr);
	
	var arrayForSort = arr.slice();

	arrayForSort.sort(function(a,b){
	    var explodeKeyA = a.date.split('.');
	    var dateA = new Date(explodeKeyA[2], explodeKeyA[1] - 1, explodeKeyA[0]);

	    var explodeKeyB = b.date.split('.');
	    var dateB = new Date(explodeKeyB[2], explodeKeyB[1] - 1, explodeKeyB[0]);

	  	return dateA - dateB;
	});

	console.log(arrayForSort);
})();