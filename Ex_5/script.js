'use strict';

(function() {
	var obj = {
	  "parts": [
	    {
	      "classification": "engine",
	      "id": "warp",
	      "price": 25
	    },
	    {
	      "classification": "engine",
	      "id": "fusion",
	      "price": 50
	    },
	    {
	      "classification": "body",
	      "id": "aluminium",
	      "price": 50
	    },
	    {
	      "classification": "body",
	      "id": "titanium",
	      "price": 120
	    },
	    {
	      "classification": "power generator",
	      "id": "nuclear",
	      "price": 200
	    },
	    {
	      "classification": "power generator",
	      "id": "solar",
	      "price": 50
	    }
	  ],
	  "parts compatibility": {
	    "nuclear:power generator": [
	      "titanium:body",
	      "warp:engine",
	      "fusion:engine"
	    ],
	    "solar:power generator": [
	      "titanium:body",
	      "aluminium:body",
	      "warp:engine"
	    ]
	  }
	};

	var searchInParts = (function(id, classification) {
		for (var key in obj.parts) {
		    if (!obj.parts.hasOwnProperty(key)) {
		    	continue;
		    }
		    if (obj.parts[key].id == id && obj.parts[key].classification) {
		    	return parseInt(obj.parts[key].price);
		    	break;
		    }
		}
	});

	for (var key in obj['parts compatibility']) {
	    if (!obj['parts compatibility'].hasOwnProperty(key)) {
	    	continue;
	    }
		var titleArray = [];
		var splitKey = key.split(':');
		titleArray.push(splitKey[0].charAt(0).toUpperCase() + splitKey[0].slice(1)+' '+splitKey[1]);
		var price = searchInParts(splitKey[0], splitKey[1]);

		for (var i = 0; i < obj['parts compatibility'][key].length; i++) {
			splitKey = obj['parts compatibility'][key][i].split(':');
			titleArray.push(splitKey[0]+' '+splitKey[1]);
			price += searchInParts(splitKey[0], splitKey[1]);
		}

		console.log(titleArray.join(', ')+'. Price: '+price);
	}

})();