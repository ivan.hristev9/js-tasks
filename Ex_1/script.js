'use strict';

(function() {
	var container = document.getElementById('container');
	var firstSelect = container.getElementsByClassName("first-select")[0];
	var secondSelect = container.getElementsByClassName("second-select")[0];

	var textField = document.getElementById('textField');
	var firstFalseOption = textField.getElementsByClassName("first-false-option")[0];
	var secondFalseOption = textField.getElementsByClassName("second-false-option")[0];
	var secondTrueOption = textField.getElementsByClassName("second-true-option")[0];

	firstSelect.onchange = function(){
		if (this.value == 1) {
			firstFalseOption.style.display = 'none';
		} else {
			firstFalseOption.style.display = 'inline-block';
		}
	};
	secondSelect.onchange = function(){
		if (this.value == 1) {
			secondFalseOption.style.display = 'none';
			secondTrueOption.style.display = 'inline-block';
		} else {
			secondFalseOption.style.display = 'inline-block';
			secondTrueOption.style.display = 'none';
		}
	};
})();