'use strict';

(function() {
	var palindrome = (function (str) {
	  var lowRegStr = str.toLowerCase();
	  var reverseStr = lowRegStr.split('').reverse().join(''); 
	  return reverseStr === lowRegStr;
	});
	console.log(palindrome("12321"));
})();