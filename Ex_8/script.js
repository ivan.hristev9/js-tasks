'use strict';

(function() {
	var firstState = false;
	var secondState = false;

	var promiseFirst = new Promise(function(resolve, reject) {
	  	setTimeout(() => resolve("first done"), 5000);
	});
	var promiseSecond = new Promise(function(resolve, reject) {
	  	setTimeout(() => resolve("second done"), 7000);
	});
	var promiseThird = new Promise(function(resolve, reject) {
	  	var interval = setInterval(function() {
	  		if (firstState && secondState) {
				resolve("third done");
				clearInterval(interval);
		  	}
		}, 500);
	});

	promiseFirst.then(function(value) {
	  console.log(value);
	  firstState = true;
	});
	promiseSecond.then(function(value) {
	  console.log(value);
	  secondState = true;
	});
	promiseThird.then(function(value) {
	  console.log(value);
	});
})();