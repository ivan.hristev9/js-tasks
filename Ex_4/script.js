'use strict';

(function() {
	const tree = {
		valueNode: 3,
		next: [{
					valueNode: 1,
					next: null
				},
				{
					valueNode: 3,
					next: null
				},
				{
					valueNode: 2,
					next: null
				},
				{
					valueNode: 2,
					next: [
						{
							valueNode: 1,
							next: null
						},
						{
							valueNode: 5,
							next: null
						}
					]
				}]
	};

	var getTreeSum = (function(param) {
		var sum = parseInt(param.valueNode);

		if (param.next && typeof param.next === 'object') {
			for (var key in param.next) {
				sum += getTreeSum(param.next[key]);
			}
		}
		
		return parseInt(sum);
	});

	console.log(getTreeSum(tree));
})();